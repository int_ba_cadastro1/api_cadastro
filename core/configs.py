from pydantic import BaseSettings
from sqlalchemy.ext.declarative import declarative_base


class Settings(BaseSettings):
    """
    Configurações gerais usadas na aplicação
    """
    API_V1_STR: str = '/api/v1'

    # credencias geradas pelo render colque a do banco que vocês criaram "postgresql+asyncpg://usuario do banco:senha@postgres.render.com:5432/nome da base de dados"
    DB_URL: str = "postgresql+asyncpg://cadastro_21204276_user:0zlkmonajJbo2z9knS6Lwf5mYAOHkOm5@postgres.render.com:5432/cadastro_21204276"
    DBBaseModel = declarative_base()

    class Config:
        case_sensitive = True


settings = Settings()
